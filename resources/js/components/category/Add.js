import React, { Component } from 'react';

export default class Add extends Component {
    constructor(){
        super();
        this.onchangeCategoryName = this.onchangeCategoryName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state={
            category_name:''
        }
    }

    onchangeCategoryName(e){
        this.setState({
            category_name:e.target.value
        });
    }

    onSubmit(e){
        e.preventDefault();
        const category = {
            category_name : this.state.category_name
        }

        axios.post('http://reactlara.me/category/store', category).then(res=>{
            console.log(res.data)});
    }
    render() {
        return (
            <div>
                <p>This is Category Add page.</p>

                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Category Name</label>
                        <input type="text"
                               className="form-control"
                               id="category_name"
                               aria-describedby="emailHelp"
                               placeholder="Enter Category Name"
                               value={this.state.category_name}
                               onChange={this.onchangeCategoryName}
                        />
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        );
    }
}
